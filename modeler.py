import torch
from PIL import Image
import matplotlib.pyplot as plt
from matplotlib import patches
from torchvision import transforms as T

######################################################################
# Название модели
MODEL_FILE = 'model_backbone8.model'
# Названия классов
CLASSES = ['open', 'short', 'mousebite', 'spur', 'copper', 'pin-hole'] 
# Цвета классов
colors = ['r', 'g', 'b', 'c', 'm', 'y']
# Уровень отсеивания (минимальная точность для распознования)
THRESHOLD = 0.6
# Размер шрифта
FONT_SIZE = 20
######################################################################

# Выбор устройства (если есть )
DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
transforms = T.Compose([T.ToTensor()])
model = torch.load(MODEL_FILE, map_location=torch.device(DEVICE))


def get_prediction(img_path: str, threshold: float):
    """ Делает предсказание

    Arguments:
        img_path {str} -- Путь к файлу
        threshold {float} -- Уровень отсеивания

    Returns:
        Координаты и названия классов
    """
    img = transforms(Image.open(img_path).convert('RGB')).to(DEVICE)
    pred = model([img])  # Загружаем изображение в модель

    return [((b[0], b[1], b[2], b[3]), CLASSES[l])
            for l,b,s in zip(pred[0]['labels'].cpu().numpy(),
                             pred[0]['boxes'].cpu().detach().numpy(),
                             pred[0]['scores'].cpu().detach().numpy()) if s > threshold
    ]


def fig_writer(img_path: str, threshold: float = THRESHOLD, font_size: int = FONT_SIZE):
    """ Обрабатывает картинку и обозначает найденное

    Arguments:
        img_path {str} -- Путь к картинке

    Keyword Arguments:
        threshold {float} -- Порог отсеивания (default: {THRESHOLD})
        font_size {int} -- Размер шрифта (default: {FONT_SIZE})
    """
    ret = get_prediction(img_path, threshold)  # Получаем предсказание
    fig = plt.figure()  # Создаем полотно
    ax = fig.add_axes([0, 0, 1, 1])  # Ставим туда оси
    img = Image.open(img_path).convert('RGB')  # Открываем изображение
    plt.imshow(img)  # Наносим его на полотно

    text = 'xmin\tymin\txmax\tymax\tclass\n'
    for box, cls in ret:
        xmin, ymin, xmax, ymax = box  # Распаковываем координаты
        text += f'{xmin}\t{ymin}\t{xmax}\t{ymax}\t{cls}\n'
        width = xmax - xmin
        height = ymax - ymin

        # Выбираем цвет и делаем надпись
        edgecolor = colors[CLASSES.index(cls)]
        ax.annotate(cls, xy=(xmax, ymin), color=edgecolor, fontsize=font_size)

        # И прямоугольник
        rect = patches.Rectangle((xmin, ymin), width, height, edgecolor=edgecolor, facecolor='none')
        ax.add_patch(rect)

    # Записываем координаты
    with open(".".join(img_path.split('.')[:-1]) + '_positions.txt', 'w') as f:
        f.write(text)
    # Сохраняем картинку
    plt.savefig(".".join(img_path.split('.')[:-1]) + '_result.jpg')
    plt.close()


# Показываем программе что готовность
print('#Ready!')
while True:
    print('##Waiting')
    p = input()
    try:
        fig_writer(p)
        print('###Success!')
    except Exception as exp:
        print('####Error:', exp)
