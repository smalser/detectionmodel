using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.IO;

/*
 * Пример выполнения кода для Main
class Program
{
    static PythonProcess python;
    static void Main(string[] args)
    {
        // Загружаем питон(один раз)
        python = new PythonProcess();

        // Посылаем сигнал с возможной картинкой
        NetResult res1 = python.Process("D:/Pa6oTa/Stash/src/1.jpg");
        Console.WriteLine(res1.success);
        // Тут мы получаем объект NetResult с success=true, картинкой в image и позициями в labels

        // Посылаем сигнал с картинкой которой нет
        NetResult res2 = python.Process("D:/Pa6oTa/Stash/src/3.jpg");
        Console.WriteLine(res2.success);
        Console.WriteLine(res2.error);
        // А тут получаем результат с success = false и ошибкой в error
    }
}*/

class PythonProcess
{
    static Process process = new Process();
    static public bool ready = false,
        success = true;
    static public string error = "";

    public PythonProcess()
    {
        process.StartInfo = new ProcessStartInfo()
        {
            WorkingDirectory = "D:/Pa6oTa/Stash/src/",  // Папка к репозиторию
            FileName = "C:/anaconda3/python",           // Путь к питону (к анаконде)
            Arguments = "modeler.py",

            UseShellExecute = false,
            CreateNoWindow = true,
            RedirectStandardInput = true,
            RedirectStandardOutput = true,
            StandardOutputEncoding = Encoding.Default,
        };
        process.OutputDataReceived += Output;

        //Стартуем процесс
        process.Start();

        //Начинаем читать выходные потоки
        process.BeginOutputReadLine();

        Wait();
    }

    public NetResult Process(string abs_path, int tick_time = 10)
    {
        success = false;
        ready = false;
        error = "";
        Write(abs_path);
        Wait(tick_time);
        return new NetResult(success, error, abs_path);
    }

    void Wait(int millisecconds = 10)
    {
        while (!ready)
        {
            Thread.Sleep(millisecconds);
        }
    }
    void Write(string data)
    {
        process.StandardInput.WriteLine(data);
    }

    void Output(object sender, DataReceivedEventArgs e)
    {
        var data = e.Data.Trim();

        if (data == "##Waiting") ready = true;
        else if (data == "###Success!") success = true;
        else if (data.Contains("####Error:")) error = new string(data.Skip(11).ToArray());  // Заполняем ошибку

        Console.WriteLine("'" + data + "'");
    }
}

class NetResult
{
    public bool success = false;
    public string error = "";

    public Image image = null;
    public Label[] labels = null;

    public NetResult(bool success, string error, string path)
    {
        this.success = success;
        this.error = error;
        if (success)
        {
            loadpic(path);
            loadtext(path);
        }
    }

    void loadpic(string path)
    {
        var splits = path.Split('.');
        image = Image.FromFile(string.Join("", splits.Take(splits.Length - 1).ToArray()) + "_result.jpg");
    }

    void loadtext(string path)
    {
        var splits = path.Split('.');
        var lines = File.ReadAllLines(string.Join("", splits.Take(splits.Length - 1).ToArray()) + "_positions.txt");
        labels = lines.Skip(1).TakeWhile(x => x.Trim() != "").Select(x => new Label(x)).ToArray();
    }

}

class Label
{
    public float xmin, ymin, xmax, ymax;
    public string label;

    public Label(string line)
    {
        var l = line.Trim().Replace('.',',').Split('\t');
        
        xmin = float.Parse(l[0]);
        ymin = float.Parse(l[1]);
        xmax = float.Parse(l[2]);
        ymax = float.Parse(l[3]);
        label = l[4].Trim();
    }
}